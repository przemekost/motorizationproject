package utils;

import data.Car;
import data.Vehicle;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Przemysław on 2017-05-01.
 */
public class SaveInputToFile {




    public void save(List<Vehicle> carList) throws FileNotFoundException {
        final String directory = "C:\\myJavaProjects\\Motorization\\src\\resources\\Cars.txt";
        File file = new File(directory);

        PrintWriter printWriter = new PrintWriter("Cars.txt");

        if (carList != null) {
            for (int i = 0; i < carList.size(); i++) {
                printWriter.println(carList.get(i));
            }
        }else{
            printWriter.println("0 cars on store");
        }


        printWriter.close();


    }
}
