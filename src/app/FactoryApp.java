package app;

import data.Car;
import data.Motorcycle;
import data.Vehicle;
import utils.DataReader;
import utils.SaveInputToFile;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Przemysław on 2017-03-09.
 */
public class FactoryApp {

    public static void main(String[] args) throws FileNotFoundException {
        final String appName = "CarFactory v0.1";

        List<Vehicle> vehicleList = new ArrayList<>();
        DataReader dR = new DataReader();

        System.out.println("Application name: " + appName);
        System.out.println("Create a vehicle: ");
        vehicleList.add(dR.readAndCreateCar());

        System.out.println("Create motorcycle: ");
        vehicleList.add(dR.readAndCreateMotorcycle());

        dR.close();


        SaveInputToFile save = new SaveInputToFile();
        save.save(vehicleList);
    }
}

